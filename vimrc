syntax on
set nocompatible

" Attempt to determine the type of a file based on its name and possibly its
" contents.  Use this to allow intelligent auto-indenting for each filetype,
" and for plugins that are filetype specific.
filetype indent plugin on

" Enable syntax highlighting
set nocompatible
syntax on

set hidden

" Better command-line completion
set wildmenu

" Show partial commands in the last line of the screen
set showcmd

set autoindent
set nosmartindent

" Stop certain movements from always going to the first character of a line.
" While this behaviour deviates from that of Vi, it does what most users
" coming from other editors would expect.
set nostartofline

" Go to last position when opening a previously edited file
if has("autocmd")
    au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
endif

au BufRead,BufNewFile Vagrantfile set ft=ruby
au BufRead,BufNewFile *.json set ft=json

if v:version >= 900
    " I'd have the following on all versions, but it's completely different
    " on earlier versions. As they all are!?
    colorscheme koehler
else
    colorscheme slate
endif

" NERDTree
nmap <C-n> :NERDTreeToggle<CR>

" Use ripgrep (rg) if possible...
if executable("rg")
    set grepprg=rg\ --vimgrep\ --no-heading
    set grepformat=%f:%l:%c:%m,%f:%l:%m
endif

