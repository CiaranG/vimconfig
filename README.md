
# What?

This is my 'standard' vim config, which is obviously different to the default
one. The ways in which it's different are the subject of the next section of
this document.

# What's Different?

Lots of things that aren't documented here yet.

## Tagbar

It's [this one](https://github.com/majutsushi/tagbar).

Use :TagbarToggle to turn the tagbar on and off, there's no key assignment
currently.

## BufExplorer

It's from [here](https://github.com/jlanzarotta/bufexplorer/).

\be to open (or \bs or \bv to force horizontal/vertical split)
\bt to toggle open/closed

While exploring:
 q     - quit exploring
 enter - go to file under cursor
 f/F   - open in horizontal split above/below
 v/V   - open in vertical split left/right
 s/S   - forward backwards through sort modes
 r     - reverse sort order
 [more...](https://github.com/jlanzarotta/bufexplorer/blob/master/doc/bufexplorer.txt)

## NERDTree

Ctrl-N to toggle sidebar

## Fugitive

Staging...
  :Git
    - then use ( and ) to move between files, - to stage/unstage,
      = to expand inline file diff
    - then use cc to create a commit, :wq after editing message
    - then use - on the 'unpushed' line to push

